Movieclub
=========

The easy way to setup you server for a distributed watch party.

Thanks to Dražen Lučanin (@metakermit) for providing initial research.

Getting started
---------------

1. Get a virtual server with linux (eg. from Digital Ocean, Scaleway)
2. Install Docker, Docker Compose and ffmpeg
3. Clone this repository
4. Copy `.env.example` to `.env` and adjust variables
5. In the repository directory run `docker-compose up`
6. Copy the file to stream to the server
7. Run `export STREAM_ID=$(pwgen 6 1)`
8. Run `source .env && echo "Mobile link: https://${MOVIECLUB_HOST}/live/${STREAM_ID}"`
9. Run `source .env && echo "Web link: https://${MOVIECLUB_HOST}/#${STREAM_ID}"`
10. Preencode the file for streaming `ffmpeg -i /path/to/movie.mp4 -c:v libx264 -preset medium -maxrate 3000k -bufsize 6000k  -g 50 -c:a aac -b:a 128k -ac 2 -ar 44100 /path/to/file.flv`
    Depending on your server this will take approximately 20 - 30% of the movie's duration.
11. Distribute the links to your friends over [Signal](https://signal.org/)
12. Run `ffmpeg -re -i file.flv -c copy -f flv rtmp://localhost/stream/${STREAM_ID}`
13. Chat on Signal about the movie
14. Only stream movies that are in the public domain and don't say I didn't warn you!
15. You could start with Sergei Eisensteins classic **[Battleship Potemkin](https://archive.org/details/BattleshipPotemkin)**
 
More Infos
----------

- [FFmpeg - Encoding for Streaming Sites](https://trac.ffmpeg.org/wiki/EncodingForStreamingSites)
- [FFmpeg - How to Burn Subtitles into Video](https://trac.ffmpeg.org/wiki/HowToBurnSubtitlesIntoVideo)

Todos
-----

### Short Term

- [ ] Add per stream chat
- [x] Document pretranscoding settings
- [ ] Allow setting stylesheet for watch page
- [ ] Allow setting title for watch page
- [ ] Create stream script that returns the watch URL
- [ ] Update traefik config to 2.0


### Long Term

- [ ] Switch to bittorrent like streaming protocol (PeerTube or similar)
